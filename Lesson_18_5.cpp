#include <iostream>

class Stack
{
private:

	int size;
	int* array;
	int hight;

public:

	Stack() : size(0), array(NULL), hight(0)
	{}
	Stack(int s)  
	{
		size = s;
		array = new int[size];
		hight = 0;
	}
	void Push(int a)
	{
		array[hight] = a;
		hight++;
	}
	
	int Pop()
	{
		hight--;
		return array[hight];
	}
};


int main()
{
	int x = 0;
	int y = 0;

	std::cout << "Enter the size of the array: ";
	std::cin >> x;
	std::cout << "\n";

	Stack arr(x);

	std::cout << "Enter the number to add: ";
	std::cin >> y;
	std::cout << "\n";

	arr.Push(y);
	std::cout << "Enter the number to add: ";
	std::cin >> y;
	std::cout << "\n";
	arr.Push(y);
	std::cout << "Enter the number to add: ";
	std::cin >> y;
	std::cout << "\n";
	arr.Push(y);

	std::cout <<"Last element: "<< arr.Pop()<<"\n\n";
	
	std::cout << "The penultimate element: " << arr.Pop() << "\n\n";
	
}